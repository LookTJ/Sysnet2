/************************************************************
 * File name:                                               *
 *  httpClient.c                                            *
 *                                                          *
 * Purpose:                                                 *
 *  Creates a simple non-persistent TCP HTTP GET client. 	*
 *                                                          *
 * Authors:                                                 *
 * -----------                                              * 
 * Andrew Petrovsky                                         *
 * Taylor Lookabaugh                                        *
 *                                                          *
 * Date:                                                    *
 *  02 March 2018                                           *
 *                                                          *
 * Course:                                                  *
 *  COP4635                                                 *
 *                                                          *
 * Notes:                                                   *
 *  May cause extreme giggly girl syndrome				    *
 ***********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>

#define BYTES 1024                                                                  // Defines how many bytes the chunk of read should be


/**********************************************************
 * Function Name:                                         *
 *  InitClient                                            *
 *                                                        *
 * Description:                                           *
 *  Starts the client program by initilizing values and   *
 *  creating and connecting the sockets (local/remote     *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * argc     	  int       I        Contains number of   *
 * 										arguemnts passed  *
 * 										to matching.      *
 * argv          char**     I        Containts arguemnts  *
 * 										passed from main. *
 * socketfd       int*       O       Newly created socket *
 *                                      descriptor.       *
 * request        char**     O       Request line to send.*
 * Function Return:                                       *
 *    1 if all good else 0          					  *
 *********************************************************/
int InitClient(int *socketfd, char **request,int argc, char** argv);

/**********************************************************
 * Function Name:                                         *
 *  SendRequest                                           *
 *                                                        *
 * Description:                                           *
 *  Sends the request to the remote server.               *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * socketfd       int*       I       Socket descriptor to *
 *                                      write/read on.    *
 * request        char**     I       Request line to send.*
 * Function Return:                                       *
 *    N/A  												  *
 *********************************************************/
void SendRequest(int socketfd,char *request);

/**********************************************************
 * Function Name:                                         *
 *  main                                              	  *
 *                                                        *
 * Description:                                           *
 *  Main program driver. Calls to setup and respond to    *
 *  Requests                                              *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *  argc		  int 		I 		 Contains number of   *
 *                                      arguemnts.        *
 *  argv          char**    I        Containts arguemnts  *
 * 									    passed by user.   *
 * Function Return:                                       *
 *    N/A  												  *
 *********************************************************/
int main(int argc,char** argv){
    
    int socketfd;                                                                       // Socket file descriptor
    char *request;                                                                      // Pointer to request line
    char prompt;

    while(1)
    {
        if (InitClient(&socketfd,&request,argc,argv))                                           // Processes user input, creates socket, and connects to host
            SendRequest(socketfd,request);                                                      // Sends request and receives response
        printf("Would you like to make another connection? [y/n]:");                            // Asks if another connection should be attempted
        scanf(" %s",&prompt);                                                                   // Gets the values
        if (strcmp(&prompt,"y"))                                                                // Compares the value
            exit(0);                                                                            // Exits if a new connection is not desired
        argc = 1;
    };

    return 0;                                                                           // If it comes to this point, we are home free.
    
}

int InitClient(int *socketfd, char **request,int argc, char** argv)
{
    struct addrinfo client_criteria, *client_addr, *temp_addr;                          // Declare addrinfo structures used for getting address
    memset(&client_criteria, 0, sizeof(client_criteria));                               // Cleares the client_criteria and sets values to 0
    
    char input_buff[100];                                                               // Declares input buffer
    char path[100];                                                                     // Declares path buffer
    char *address[2];                                                                   // Declares address buffer
    
    if (argc < 3 || argc > 3)                                                           // Checks to see if the user inserted arguments upon execution
    {
        printf("HTTP Client\nUsage:<address:port> <path>\nExample: 127.0.0.0.1:60023 /images/image1.jpg\n:");                       // Prints directions if no arguemnts specificed
        scanf("%s %s",input_buff,path);
        address[0] = strtok(input_buff,":");                                                                                        // Splits the address into IP and port
        address[1] = strtok(NULL," ");                                                                                              //          ^               ^
    }
    else 
    {
        address[0] = strtok(argv[1],":");                                                                                            // Splits the address into IP and port
        address[1] = strtok(NULL," ");                                                                                               //          ^               ^
        if (sizeof(path) >= sizeof(argv[2])){                                                                                         // Verifies to make sure the path isn't longer than 100 characters 
            strcpy(path,argv[2]);                                                                                                    // Copies the path
            strcat(path,"\0");
        }
        else
        {
            printf("Size of the path is too long. Exiting.\n");                                                                      // Error message if the path is too long
            return 0;                                                                                                                 // Runs away 
        }
    }
    
    printf("Attempting to connect to %s on port %s and retrieve %s\n",address[0],address[1],path);                                  // Prints message to screen on the parameters it has gathered
    *request = calloc(strlen("GET ")+strlen(path)+strlen("HTTP/1.1\n\n  "),1);                                                        // Allocates enough space for the request line
    sprintf(*request,"GET %s HTTP/1.1\n\n",path);                                                                                   // Puts together the request line
    
    client_criteria.ai_family = AF_INET;                                                                                            // Defines the type of address required for connection
    client_criteria.ai_socktype = SOCK_STREAM;                                                                                      //                  ^               ^
    client_criteria.ai_flags = 0;                                                                                                   //                  ^               ^
    
    if (getaddrinfo(address[0],address[1],&client_criteria,&temp_addr) != 0)                                                        // Gathers addresses that match the criteria
    { 
        printf("Unable to get correct address\n");                                                                                  // Prints error and quits if can't find match
        return 0;                                                                                                                   // 				^			^
    }
    for (client_addr = temp_addr; client_addr != NULL; client_addr = client_addr->ai_next)                                          // Searches the results for a working address
    {
        *socketfd = socket(client_addr->ai_family, client_addr->ai_socktype, 0);                                                    // Attempts to create a socket
        if (*socketfd == -1)                                                                                                        // Checks to see if it worked
        {
            printf("Failed to create socket with chosen address. Trying next address\n");                                           // Prints error message if it can't and continues to the next address
            continue;                                                                                                               //                  ^                   ^
        }
        printf("Socket created successfully!\n");                                                                                   // Prints a message if all went well
        if (connect(*socketfd,client_addr->ai_addr, client_addr->ai_addrlen) != -1)                                                 // Attempts to connect to the remote host
        {
        printf("Connected successfully!\n");                                                                                        // Prints message if the attemp is successful
        break;                                                                                                                      // breaks out of the loop
        }
    }
    if (client_addr == NULL)                                                                                                        // If connection fails 
    {
        printf("Unable to connect to remote host\n");                                                                               // Prints error message and exits
        return 0;                                                                                                                    //          ^           ^
    }
    freeaddrinfo(temp_addr); 
    return 1;                                                                                                       // Clears the temp addrinfo struct 
}

void SendRequest(int socketfd,char *request)                                                                                        
{
    int bytes_read = 1;                                                                                                             // Declares and initializes the bytes read variable
    char receive_buff[999999];                                                                                                      // Declares the receive buffer
    
    if (write(socketfd,request,strlen(request)) > 0)                                                                                // Writes the request message to the open socket
    printf("Request sent!\n");                                                                                                      // Prints message out to screen 
    shutdown(socketfd,SHUT_WR);                                                                                                     // Cuts off the send line (goes from full-duplex to simplex)
    while (bytes_read > 0)                                                                                                          // While there are bytes still needing to read 
    {
        bytes_read = read(socketfd,receive_buff,BYTES);                                                                             // Read the bytes and store them in buffer
        write(1,receive_buff,bytes_read);                                                                                           // Print those bytes to the screen using the write command (instead of printf for kicks)
    }
    printf("\n");                                                                                                                   // Prints a new line to make output look pretty
    close(socketfd);                                                                                                                // Closes the socket
}
