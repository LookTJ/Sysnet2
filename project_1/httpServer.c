/************************************************************
 * File name:                                               *
 *  httpServer.c                                            *
 *                                                          *
 * Purpose:                                                 *
 *  Creates a simple non-persistent TCP HTTP server. 		*
 *                                                          *
 * Authors:                                                 *
 * -----------                                              * 
 * Andrew Petrovsky                                         *
 * Taylor Lookabaugh                                        *
 *                                                          *
 * Date:                                                    *
 *  02 March 2018                                           *
 *                                                          *
 * Course:                                                  *
 *  COP4635                                                 *
 *                                                          *
 * Notes:                                                   *
 *  May cause extreme giggly girl syndrome				    *
 ***********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <fcntl.h>
#include <sys/stat.h>

#define MAX_QUEUE 10											// Defines the max connections stored via listen()
#define BYTES 1024												// Defines the size of byte chunks to send files with
#define DEF_PORT "60023"										// Defines the default port to bind server to




/**********************************************************
 * Function Name:                                         *
 *  InitServer                                            *
 *                                                        *
 * Description:                                           *
 *  Starts the server program by initilizing values and   *
 *  creating,binding, and listening to the socket.        *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * argc     	  int       I        Contains number of   *
 * 										arguemnts passed  *
 * 										to matching.      *
 * argv          char**     I        Containts arguemnts  *
 * 										passed from main. *
 * Function Return:                                       *
 *    N/A  												  *
 *********************************************************/

void InitServer(int argc, char** argv,int *socketfd,char **root);

/**********************************************************
 * Function Name:                                         *
 *  Respond                                          	  *
 *                                                        *
 * Description:                                           *
 *  Responds to a pending connection request.		      *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *  sock 		  int 		I 		Identifies the socket *
 *  								  to respond to.      *
 * Function Return:                                       *
 *    N/A  												  *
 *********************************************************/
void Respond(int sock,char * root);

int main(int argc, char* argv[])
{
    char *root;														// Declaring variable to store root directory in
    int socketfd;													// Declaring variable for socket descriptor
	struct sockaddr_in clientaddr;								    // Create a client address data structure
	socklen_t addrlen;											    // And client address 
	InitServer(argc,argv,&socketfd,&root);							// Start the server to bind the sockets
	while (1)																		// While loop to listen for connections
	{																				// 				^			^
		Respond(accept (socketfd, (struct sockaddr *) &clientaddr, &addrlen),root);	// Responds to the received connection
	}																				// 				^			^

	return 0;																		// Returns out
}

void InitServer(int argc, char** argv,int *socketfd,char **root)
{
    char port[6];												// Declaring variable to store port number
	struct addrinfo serv_criteria, *serv_addr, *temp_addr;		// Declaring socket structures to find the right interface
	memset(&serv_criteria, 0, sizeof(serv_criteria));			// Setting all values to 0 so there is no unset values
    
    *root = calloc(strlen(getenv("PWD")),1);                    // Allocating enough memory to store the path
	*root = getenv("PWD");										// Uses current working directory as the server root
	if(argc == 2)												// Checking if another port was designated
		strcpy(port,argv[1]);									// Sets the port to the user input
	else														// If not
		strcpy(port,DEF_PORT);									// Sets the port to the default port
	
	serv_criteria.ai_family = AF_INET; 							// Setting up TCP settings in socket structure
	serv_criteria.ai_socktype = SOCK_STREAM;					// 				^			^
	serv_criteria.ai_flags = AI_PASSIVE;						// 				^			^

	if (getaddrinfo(NULL, port, &serv_criteria, &temp_addr) != 0)	// Retrieves all available internet addresses on computer
	{																//  matching the criteria specified above
		printf("Unable to get correct address\n");								// Prints error and quits if something wrong
		exit(1);													// 				^			^
	}															

	for (serv_addr = temp_addr; serv_addr!=NULL; serv_addr=serv_addr->ai_next)					// Cycles through all available addresses to find one it can use
	{																							// 				^			^
		*socketfd = socket (serv_addr->ai_family, serv_addr->ai_socktype, 0);					// Attempts to create a socket on the available address
		if (*socketfd == -1){																	// Checks to see if it succeded 
			printf("Failed to create socket with the chosen address. Trying next address\n");   // Prints error message if it didn't
			continue;																			// continues on to the next address structure
		}																						// 				^			^
		if (bind(*socketfd, serv_addr->ai_addr, serv_addr->ai_addrlen) != 0)					// Tries to bind the newly created socket
			printf("Failed to bind socket on current address\n");								// Prints error message if it can't
        else
			break;																				// Breaks out of loop if it can't and will exit upon the next message
	}

	if (serv_addr == NULL)																	// Checks to see if it has a valid socket/bind
	{																						// 				^			^
		printf("Unable to find an address to create a socket and bind\n");					// Prints a message and quits if not
		exit(1);																			// 				^			^
	}

	if ( listen (*socketfd, MAX_QUEUE) != 0 )													// Starts to listen for connections with a queue of MAX_QUEUE
	{																						// 				^			^
		printf("Unable to start listening\n");												// Prints error message and quits if it doesn't
		exit(1);																			// 				^			^
	}

	printf("Server started on port number: %s with path: %s as root directory.\n Awaiting new connections...\n",port,*root);					// Reporting port and current working directory
	freeaddrinfo(temp_addr);																													// Freeing the temp addrinfo struct
}

void Respond(int sock,char *root)
{
    struct stat fileSize;
    char* sendHeader;
	char msg[99999], *reqline[3], data_to_send[BYTES], path[99999];									// Setting up buffers for messages
	int rcv_flag, file, bytes_read;																	// Setting up variables
	strcpy(path, root);																				// Coppies root to path (used later)
	char* textheader = "Content-Type: text/html; charset=UTF-8\n";									// Header for error message
	char* notfoundmsg = "<!DOCTYPE html><h1><b>Error 404</b></h1><p> File not found.</p>\n";		// Body for error message
    char* okmsg = "HTTP/1.1 200 OK\n";

	memset((void*)msg, (int)'\0', 99999 );															// Clearing message buffer

	rcv_flag=recv(sock, msg, 99999, 0);																//Receiving message from connection

	if (rcv_flag<0)    																				// Checking for good transmission
		printf("Receive error\n");																	// Printing error: Failed to receive
	else if (rcv_flag==0)    																		// 				^			^
		fprintf(stderr,"Client disconnected upexpectedly.\n");										// Printing error: client closed connection
	else    
	{
		printf("\nNew connection with request message:\n%s\n\n", msg);																			// Printing received message
		reqline[0] = strtok (msg, " \t\n");															// Parsing out the request line
		if (strncmp(reqline[0], "GET\0", 4) == 0)													// Checking for GET request
		{
			reqline[1] = strtok (NULL, " \t");
			reqline[2] = strtok (NULL, " \t\n");
			if (strncmp(reqline[2], "HTTP/1.1", 8) != 0)											// Checks to verify HTTP1.1 Protocol in use
			{
                printf("Invalid request made\n");                                                   
				write(sock, "HTTP/1.1 400 Bad Request\n", 25);											// If correct protocol not in use respondes with error
			}
			else
			{
				if ( strncmp(reqline[1], "/\0", 2)==0 )												// Checks for root request
					reqline[1] = "/index.html";        												// modifies the request list to send index.html

				strcat(path,reqline[1]);															// Adds the requested file to path									

				if ( (file=open(path, O_RDONLY))!=-1 )   											// Tries to open the file
				{
                    stat(path, &fileSize);                                                                          //Gets the filesize of the requested file
                    sendHeader = calloc(strlen(okmsg)+strlen("Content-Length: \n\n")+sizeof(fileSize.st_size),1);   //Allocates the send header size
                    sprintf(sendHeader,"%sContent-Length: %d\n\n",okmsg,(int)fileSize.st_size);                         //Assembles the send header
     				printf("Sending File: %s\n", path);												// Prints the request out to screen
					//send(sock, "HTTP/1.1 200 OK\n\n", 17, 0);										// If the file is found, send OK Response
                    send(sock,sendHeader,strlen(sendHeader),0);                                     // Sends the Response + header
					while ((bytes_read = read(file, data_to_send, BYTES)) > 0)						// Sends the data as there are more bytes to send in BYTE chunks
						write (sock, data_to_send, bytes_read);										//				^				^
                    free(sendHeader);
				}
				else{																				// When the file is not found
                    printf("Requested file not found\n");                                               // Printing out file not found message
					write(sock, "HTTP/1.1 404 Not Found\n", 23); 										// Not Found response
					write(sock, textheader, strlen(textheader));										// Sends the content type header
                    sendHeader = calloc(strlen("Content-Length: \n\n")+ strlen(notfoundmsg),1);
                    sprintf(sendHeader,"Content-Length: %ld\n\n",strlen(notfoundmsg));
                    write(sock,sendHeader,strlen(sendHeader));
					//write(sock, "\n", 1);																// Sends a new line to identify body is next
					write(sock, notfoundmsg, strlen(notfoundmsg));										// Sends the Not Found message to view on screen
                    free(sendHeader);
				}
			}
		}
	}
	close(sock);
	printf("File sent, socket closed\n");																						// Closing socket
}

