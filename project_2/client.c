/************************************************************
 * File name:                                               *
 *  client.c 	                                            *
 *                                                          *
 * Purpose:                                                 *
 *  Creates a simple UDP bulletin board.            		*
 *                                                          *
 * Authors:                                                 *
 * -----------                                              * 
 * Andrew Petrovsky                                         *
 * Taylor Lookabaugh                                        *
 *                                                          *
 * Date:                                                    *
 *  17 April 2018                                           *
 *                                                          *
 * Course:                                                  *
 *  COP4635                                                 *
 *                                                          *
 * Notes:                                                   *
 * This one is dedicated to Samantha, may all grades be 100 *
 ***********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <libxml/parser.h>

#define BYTES 1024                                              // Defines the size of byte chunks to send data with
#define CLEAR '-'                                               // Defines the clear setting
#define UPDATE 'U'                                              // Defines the update setting
#define JOIN 'J'                                                // Defines the join setting
#define LEAVE 'L'                                               // Defines the leave setting
#define LOCAL "127.0.0.1"                                       // Defines the defines the localhost address
#define MSG_SIZE 5000                                           // Defines the size of the message buffer


/* Struct definition for message format in udp transmission */
typedef struct{
    char action;                                                // Action setting
    int token;                                                  // Token setting
    uint16_t port_self;                                         // Port of message sender setting
    uint16_t port_other;                                        // Port of neighbor (in most cases) setting
}udp_msg;                                                       // Type defined

/* Struct definition for message format between parent/child threads */
typedef struct{
    char action;                                                // Action setting
    int msg_number;                                             // Message number setting, indicates which message to read
    pthread_mutex_t busy;                                       // Mutex to prevent more than one operation from taking place between token exchanges.
    char msg[MSG_SIZE];                                         // Message buffer.
    char filename[100];                                         // BulletinBoard filename
}local_msg;                                                     // Type defined

/* Using an enumerated list for the menu, as well as actions. */
typedef enum{
        WRITE,                                                  // Sets to 0
        READ,                                                   // Sets to 1
        LIST,                                                   // Sets to 2 
        EXIT                                                    // Sets to 3
    }choice;


/**********************************************************
 * Function Name:                                         *
 *  InitClient                                            *
 *                                                        *
 * Description:                                           *
 *   Starts the client up by setting the arguments,       *
 *    initilize arguments, binds port, and requests       *
 *    joining up with network, be it a new network or     *
 *    existing.                                           *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * argc     	  int       I        Contains number of   *
 * 										arguemnts passed  *
 * 										to matching.      *
 * argv          char**     I        Containts arguemnts  *
 * 										passed from main. *
 * socketfd       int*      O        Socket used for UDP  *
 *                                      transmission.     * 
 * Function Return:                                       *
 *    N/A  												  *
 *********************************************************/
void InitClient(int argc, char** argv,int *socketfd);

/**********************************************************
 * Function Name:                                         *
 *  NewRing                                               *
 *                                                        *
 * Description:                                           *
 *   Sends the appropriate message, on the specified port *
 *    to join a new token ring. (Server method)           *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * argv          char**     I        Containts arguemnts  *
 *                                      for server port.  *
 * socketfd       int*      I        Socket used for UDP  *
 *                                      transmission.     * 
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void NewRing(char **argv,int socketfd);

/**********************************************************
 * Function Name:                                         *
 *  WaitonMsg                                             *
 *                                                        *
 * Description:                                           *
 *   Waits for a new message to be transmited (received)  *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * socketfd       int*      I        Socket used for UDP  *
 *                                      transmission.     * 
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void WaitOnMsg(int socketfd);

/**********************************************************
 * Function Name:                                         *
 *  PassMsg                                               *
 *                                                        *
 * Description:                                           *
 *   Transmits the message to the neighbor.               *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * socketfd       int*      I        Socket used for UDP  *
 *                                      transmission.     * 
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void PassMsg(int socketfd);

/**********************************************************
 * Function Name:                                         *
 *  PassMsg                                               *
 *                                                        *
 * Description:                                           *
 *   Parses the newly received message.                   *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * socketfd       int*      I        Socket used for UDP  *
 *                                      transmission.     * 
 * new_msg        udp_msg*  I        Newly received       *
 *                                     message.           *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void ParseMsg(int socketfd, udp_msg *new_msg);

/**********************************************************
 * Function Name:                                         *
 *  PeerLeaving                                           *
 *                                                        *
 * Description:                                           *
 *   Checks if the leaving peer is the neighbor.          *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * socketfd       int*      I        Socket used for UDP  *
 *                                      transmission.     *
 * new_msg        udp_msg*  I        Newly received       *
 *                                     message.           *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void PeerLeaving(int socketfd, udp_msg *new_msg);

/**********************************************************
 * Function Name:                                         *
 *  PeerJoining                                           *
 *                                                        *
 * Description:                                           *
 *   Processes the joining peer.                          *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * socketfd       int*      I        Socket used for UDP  *
 *                                      transmission.     *
 * new_msg        udp_msg*  I        Newly received       *
 *                                     message.           *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void PeerJoining(int socketfd, udp_msg *new_msg);

/**********************************************************
 * Function Name:                                         *
 *  ClearNextMsg                                          *
 *                                                        *
 * Description:                                           *
 *   Clears the next UDP message to be sent.              *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *    N/A                                                 *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void ClearNextMsg(void);

/**********************************************************
 * Function Name:                                         *
 *  ClearLocalMsg                                         *
 *                                                        *
 * Description:                                           *
 *   Clears the local message. (One used between threads) *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *   init          int       I        Flag to initilize   *
 *                                       the mutex.       *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void ClearLocalMsg(int init);

/**********************************************************
 * Function Name:                                         *
 *  MenuInit                                              *
 *                                                        *
 * Description:                                           *
 *   Starts the loop to show the menu.                    *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *    N/A                                                 *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void MenuInit(void);

/**********************************************************
 * Function Name:                                         *
 *  GetOption                                             *
 *                                                        *
 * Description:                                           *
 *   Gets a value from the stdin.                         *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *   flag          int       I        Flag to set a bound *
 *                                       menu range or all*
 *                                       values.          *
 * Function Return:                                       *
 *    int - Value that the user inputs.                   *
 *********************************************************/
int GetOption(int flag);

/**********************************************************
 * Function Name:                                         *
 *  Menu                                                  *
 *                                                        *
 * Description:                                           *
 *   Shows the Menu.                                      *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *    N/A                                                 *
 * Function Return:                                       *
 *    int - Returns a valid menu option, that is selected *
 *********************************************************/
int Menu(void);

/**********************************************************
 * Function Name:                                         *
 *  DoOption                                              *
 *                                                        *
 * Description:                                           *
 *   Starts the loop to show the menu.                    *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *    N/A                                                 *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void DoOption(choice option);

/**********************************************************
 * Function Name:                                         *
 *  LeaveRing                                             *
 *                                                        *
 * Description:                                           *
 *   Processes a leave request to exit.                   *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * socketfd       int*      I        Socket used for UDP  *
 *                                                        *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void LeaveRing(int socketfd);

/**********************************************************
 * Function Name:                                         *
 *  JoinExisting                                          *
 *                                                        *
 * Description:                                           *
 *   Sends the appropriate message, on the specified port *
 *    to join an existing token ring. (p2p method)        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * argv          char**     I        Containts arguemnts  *
 *                                      for server port.  *
 * socketfd       int*      I        Socket used for UDP  *
 *                                      transmission.     * 
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void JoinExisting(char **argv, int socketfd);

/**********************************************************
 * Function Name:                                         *
 *  MsgCount                                              *
 *                                                        *
 * Description:                                           *
 *   Counts the number of messages in the bulletin board. *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *    N/A                                                 *
 * Function Return:                                       *
 *    int - number of messages.                           *
 *********************************************************/
int MsgCount(void);

/**********************************************************
 * Function Name:                                         *
 *  GetMsg                                                *
 *                                                        *
 * Description:                                           *
 *   Prompts the user for message input.                  *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *    N/A                                                 *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void GetMsg(void);

/**********************************************************
 * Function Name:                                         *
 *  WriteMsg                                              *
 *                                                        *
 * Description:                                           *
 *   Writes the message from buffer into file.            *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *    N/A                                                 *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void WriteMsg(void);

/**********************************************************
 * Function Name:                                         *
 *  CheckFile                                             *
 *                                                        *
 * Description:                                           *
 *   Checks if the specified file exists.                 *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *    N/A                                                 *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void CheckFile(void);

/**********************************************************
 * Function Name:                                         *
 *  GetNum                                                *
 *                                                        *
 * Description:                                           *
 *   Prompts the user for a number                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *    N/A                                                 *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void GetNum(void);

/**********************************************************
 * Function Name:                                         *
 *  ReadOrListMsg                                         *
 *                                                        *
 * Description:                                           *
 *   Reads a message from the bulletin board file.        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 *   init          int       I        Flag to option      *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void ReadOrListMsg(int option);

/**********************************************************
 * Function Name:                                         *
 *  SanityCheck                                           *
 *                                                        *
 * Description:                                           *
 *  Checks the arguments are proper.                      *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * argc           int       I        Contains number of   *
 *                                      arguemnts passed  *
 *                                      to matching.      *
 * argv          char**     I        Containts arguemnts  *
 *                                      passed from main. *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void SanityCheck(int argc, char **argv);



/* GLOBAL VARIABLES */
uint16_t NEIGHBOR;                                                  // Neighbors port.
uint16_t MYPORT;                                                    // Clients port.
int TOKEN;                                                          // Token value.
udp_msg NEXT_MSG;                                                   // Next message to be sent to neighbor.
local_msg LOCAL_MSG;                                                // Message between parent/thread.

int main(int argc, char* argv[])
{
    SanityCheck(argc,argv);                                         // Verifies arguments and such
    int socketfd;													// Declaring variable for socket descriptor
    pthread_t thread;                                               // Declaring thread identifier variable.
    pthread_attr_t attr;                                            // Declaring thread attributes variable.
    pthread_attr_init(&attr);                                       // Initilizing thread attributes variable.

    InitClient(argc,argv,&socketfd);                                // Starting up the client
    pthread_create(&thread,&attr,(void *) MenuInit,NULL);           // Spawning a thread to drive the interface.

    while(1)                                                        // Starting a forever loop <3
    {

        if(TOKEN)                                                   // Check for the token
        {
            NEXT_MSG.token = 1;                                     // Sets the next message to pass the token.
            if(LOCAL_MSG.action != CLEAR){                          // Checks for action needing to be taken
                if(LOCAL_MSG.action == WRITE)                       // Checks if a message needs to be written to file
                    WriteMsg();                                     // Write the buffered message into the bb file

                else if(LOCAL_MSG.action == LEAVE)                  // Check if the user wants to quit
                {
                    LeaveRing(socketfd);                            // Sends the neighbor a message about client quitting.
                    break;                                          // Breaks out of the forever loop
                }   
                else if(LOCAL_MSG.action == READ)                   // Checks if a message needs to be read.
                    ReadOrListMsg(0);                               // Reads the message and prints it out onto screen.

                else if(LOCAL_MSG.action == LIST)                   // Checks for a list action.
                    ReadOrListMsg(1);                               // Prints the valit message range to screen.
            }

            PassMsg(socketfd);                                      // Passes the message to neighbor
            TOKEN = 0;                                              // Releases the token
            NEXT_MSG.token = 0;                                     // Sets the next message token to 0
        }
        WaitOnMsg(socketfd);                                        // Waits for the next message.
    }
    
    pthread_join(thread,NULL);                                      // Rejoins the thread
    // pthread_mutexattr_destroy(&attr);                               // Destroys the thread attribute
    pthread_mutex_destroy(&LOCAL_MSG.busy);                         // Destroys the mutex
    printf("Client exiting....\n");                                 // Prints final message.

	return 0;														// Returns out
}

void InitClient(int argc, char** argv,int *socketfd)
{
  char port[6];                                                 // Port buffer
	struct addrinfo serv_criteria, *serv_addr, *temp_addr;		// Declaring socket structures to find the right interface
	memset(&serv_criteria, 0, sizeof(serv_criteria));			// Setting all values to 0 so there is no unset values

    ClearNextMsg();                                             // Clears the next message
    ClearLocalMsg(1);                                           // Clears local message and initilizes the mutex


	if(argc == 4)												// Checking if making a new connection
		strcpy(port,argv[1]);									// Sets the port (self) 
	else
		strcpy(port,argv[2]);									// Sets the port (self)
	
	serv_criteria.ai_family = AF_INET; 							// Setting up TCP settings in socket structure
	serv_criteria.ai_socktype = SOCK_DGRAM;						// 				^			^
	serv_criteria.ai_flags = AI_PASSIVE;						// 				^			^
    
    MYPORT = htons(atoi(port));                                 // Converts port to network order.
	if (getaddrinfo(NULL, port, &serv_criteria, &temp_addr) != 0)	                            // Retrieves all available internet addresses on computer
	{																                            //  matching the criteria specified above
		printf("Unable to get correct address\n");								                // Prints error and quits if something wrong
		exit(1);													                            // 				^			^
	}															

	for (serv_addr = temp_addr; serv_addr!=NULL; serv_addr=serv_addr->ai_next)					// Cycles through all available addresses to find one it can use
	{																							// 				^			^
		*socketfd = socket (serv_addr->ai_family, serv_addr->ai_socktype, 0);					// Attempts to create a socket on the available address
		if (*socketfd == -1){																	// Checks to see if it succeded 
			printf("Failed to create socket with the chosen address. Trying next address\n");   // Prints error message if it didn't
			continue;																			// continues on to the next address structure
		}																						// 				^			^
		if (bind(*socketfd, serv_addr->ai_addr, serv_addr->ai_addrlen) != 0)					// Tries to bind the newly created socket
			printf("Failed to bind socket on current address\n");								// Prints error message if it can't
        else
			break;																				// Breaks out of loop if it can't and will exit upon the next message
	}

	if (serv_addr == NULL)																	    // Checks to see if it has a valid socket/bind
	{																					     	// 				^			^
		printf("Unable to find an address to create a socket and bind\n");				       	// Prints a message and quits if not
		exit(1);																	     		// 				^			^
	}
	printf("Client started on port number: %s\n",port);		                                    // Reporting port and current working directory

	freeaddrinfo(temp_addr);                                                                    // Frees the search structs.

    if(argc == 5)                                                                               // Identifies a new ring must be formed
    {	
        strcpy(LOCAL_MSG.filename,argv[4]);														// Copies the bb filename 
        NewRing(argv,*socketfd);                                                                // Sends a message to the server to form a new ring
        CheckFile();                                                                            // Checks to see if the file exists.
    }
    else 
    {
        strcpy(LOCAL_MSG.filename,argv[3]);                                                     // Copies the bb filename 
        JoinExisting(argv,*socketfd);                                                           // Sends a message to a peer.
    }
}


void NewRing(char **argv,int socketfd)
{
    struct sockaddr_in client_addr;                                                             // Create a client address data structure
    socklen_t client_addrlen;                                                                   // Create a size variable for client structure
    udp_msg msg_in;                                                                             // Declare a new msg buffer
    ssize_t bread;                                                                              // Declare a variable to store the bytes read (bread..get it...ha!)

    inet_pton(AF_INET, LOCAL, &client_addr.sin_addr);                                           // Setting the address to loopback for localhost
    client_addr.sin_port = htons(atoi(argv[3]));                                                // Setting the port to connect to i.e the server
    client_addr.sin_family = AF_INET;                                                           // Setting the family name (ipv4 only one choice)
    client_addrlen = sizeof(client_addr);                                                       // Setting the size of the structure
    
    bread = sendto(socketfd, "Hi!", strlen("Hi!"), 0,(struct sockaddr *) &client_addr,client_addrlen);                                                      // Sending greeting to server
    if (bread <= 0)                                                                                                                                         // If sending error
    {
        printf("Failed to send\n");                                                                                                                         // Prints fail message.
        exit(-1);                                                                                                                                           // Exits.
    }
    
    bread = recvfrom(socketfd, &msg_in, sizeof(msg_in), 0, (struct sockaddr *) &client_addr, &client_addrlen);                                              // Awaits a response
    if (bread <= 0)                                                                                                                                         // Checks if received properly
    {
        printf("Failed to receive properly\n");                                                                                                             // Errors message
        exit(-1);                                                                                                                                           // Exits.
    }
    
    if(msg_in.token == 1)                                                                                                                                   // Checks for token in received message
        TOKEN = 1;                                                                                                                                          // Sets token flag.
    NEIGHBOR = msg_in.port_other;                                                                                                                           // Sets the received port.
}

void WaitOnMsg(int socketfd)                                                                                            
{
    struct sockaddr_in client_addr;                                                                                                                         // Declare a client address data structure
    socklen_t client_addrlen;                                                                                                                               // Declare a size variable for client structure
    udp_msg msg_in;                                                                                                                                         // Declare a new msg buffer
    ssize_t bytes_read;                                                                                                                                     // Declare bytes read variable

    bytes_read = recvfrom(socketfd, &msg_in, sizeof(msg_in), 0, (struct sockaddr *) &client_addr, &client_addrlen);                                         // Receive messsage
    
    if(bytes_read <= 0)                                                                                                                                     // Check for error
    {
        printf("Error in received message\n");                                                                                                              // Print message
        exit(-1);                                                                                                                                           // Exit
    }
    ParseMsg(socketfd, &msg_in);                                                                                                                            // Process message 
}

void PassMsg(int socketfd)
{
  struct sockaddr_in client_addr;                                                                                               // Declare a client address data structure
  socklen_t client_addrlen;                                                                                                     // Declare size variable for address structure
  ssize_t bytes_sent;                                                                                                           // Declare byte size variable

  client_addr.sin_family = AF_INET;                                                                                             // Define the family (ipv4)
  client_addr.sin_port = NEIGHBOR;                                                                                              // Define the port
  inet_pton(AF_INET, LOCAL, &client_addr.sin_addr);                                                                             // Define the address 
  client_addrlen = sizeof(client_addr);                                                                                         // Define the structure size

  bytes_sent = sendto(socketfd, &NEXT_MSG, sizeof(NEXT_MSG), 0,(struct sockaddr *) &client_addr,client_addrlen);                // Send message
  
  if(bytes_sent <= 0)                                                                                                           // Check for send errors
  {
    printf("Failed to send\n");                                                                                                 // Print message 
    exit(-1);                                                                                                                   // Exit
  }
}

void ParseMsg(int socketfd,udp_msg *new_msg)
{
    TOKEN = new_msg->token;                                                                                                     // Setting token
    switch(new_msg->action)                                                                                                     // Check action of message
    {
        case '-':                                                                                                               // If no action, take no action
          break;

        case 'U':
          NEIGHBOR = new_msg->port_other;                                                                                       // If update process the update.
          ClearNextMsg();                                                                                                       // Clears the next message so it doesn't propegate
          break;

        case 'L':
          PeerLeaving(socketfd, new_msg);                                                                                       // Process a leaving peer
          break;

        case 'J':
          PeerJoining(socketfd, new_msg);                                                                                       // Process a joining peer
          break;

        default:
            printf("Error parsing UDP message.\n");
  }
}

void PeerLeaving(int socketfd, udp_msg *new_msg)
{
    if(new_msg->port_self == NEIGHBOR)                                                                                          // Checks if the peer that is leaving is this clients neighbor
        NEIGHBOR = new_msg->port_other;                                                                                         // Updates port if that is true
    else
    {
        NEXT_MSG.action = new_msg->action;                                                                                      // If not, message contents are passed
        NEXT_MSG.port_self = new_msg->port_self;                                                                                // ^^
        NEXT_MSG.port_other = new_msg->port_other;                                                                              // ^^
        PassMsg(socketfd);                                                                                                      // Sending message
    }                                                                                                      
    ClearNextMsg();                                                                                                             // Clearing next message.
}

void PeerJoining(int socketfd, udp_msg *new_msg)
{
    do                                                                                                                          
    {
        WaitOnMsg(socketfd);                                                                                                    // Waits until a token message is received
    }while(TOKEN != 1);
    NEXT_MSG.token = 1;                                                                                                         // Updates the client settings to reflect.
    NEXT_MSG.action = UPDATE;                                                                                                   //  ^^^
    NEXT_MSG.port_other = NEIGHBOR;                                                                                             //  ^^^^
    NEIGHBOR = new_msg->port_self;                                                                                              // ^^^^^
    PassMsg(socketfd);                                                                                                          // passes the token to the new neighbor
    ClearNextMsg();                                                                                                             // Clears the next message
}

void ClearNextMsg(void)
{
    NEXT_MSG.token = 0;                                                                                                         // Defines the values to their empty condition
    NEXT_MSG.action = CLEAR;                                                                                                    // ^    ^   ^
    NEXT_MSG.port_self = 0;                                                                                                     // ^    ^   ^
    NEXT_MSG.port_other = 0;                                                                                                    // ^    ^   ^
}
void ClearLocalMsg(int init)
{
    LOCAL_MSG.action = CLEAR;                                                                                                    // Defines the values to their empty condition
    LOCAL_MSG.msg_number = -1;                                                                                                   // ^    ^   ^
    strcpy(LOCAL_MSG.msg,"\0");                                                                                                  // ^    ^   ^
    if(init)                                                                                                                     // If the init flag is set the mutex is initilized
        pthread_mutex_init(&LOCAL_MSG.busy,NULL);
    else                                                                                                                         // Else the flag is unlocked
        pthread_mutex_unlock(&LOCAL_MSG.busy);

}

void MenuInit(void)
{
    choice option;                                                                                                               // Declarin option variable 
    option = (choice) Menu();                                                                                                    // Printing menu and getting choice
    while(option != EXIT)                                                                                                        // Until Exit loop
    {
        pthread_mutex_trylock(&LOCAL_MSG.busy);                                                                                  // Verifying one action takes place at a time
        DoOption(option);                                                                                                        // Performs action
        option = (choice) Menu();                                                                                                // Repeat
    }
    DoOption(EXIT);                                                                                                              // Perform exit 
    printf("Exiting..Please wait for token transmission and shutdown.\n");                                                       // Notifies the user of thread exit
    pthread_exit(NULL);                                                                                                          // Thread exit. (ZOMBIE eh eh eh)
}

int Menu(void)
{
    int option;                                                                                                                  // Declares option variable

    do {
        printf("%i. appends a new message to the end of the message board.\n", WRITE);                                          // MENU printout
        printf("%i. read a particular message from the message board using a seq number.\n", READ);                             // MENU printout
        printf("%i. displays the range of valid seq numbers of messages posted.\n", LIST);                                      // MENU printout
        printf("%i. closes the message board and leave the token ring.\n", EXIT);                                               // MENU printout
        printf("Selection: ");                                                                                                  // MENU printout
        option = GetOption(1);                                                                                                  // Gets user input
    } while (option < WRITE || option > EXIT);                                                                                  // Prints all of it until a valid entry is made
    return option;                                                                                                              // Returns value
}
int GetOption(int flag)
{
        char buffer[10];                                                                                                        // Defines buffer
        char *p;                                                                                                                // defines the char pointer (literal storage)
        long int i;                                                                                                             // Defines a long int....because a normal one won't do - Taylor..

        if (fgets(buffer, sizeof(buffer), stdin) != NULL)                                                                       // Uses fgets to gather 10 characters from stdin
        {
                i = strtol(buffer, &p, 10);                                                                                     // Converts the buffer to literal

                if (buffer[0] != '\n' && (*p == '\n' || *p == '\0')                                                             // Verifies the entry is not a unwanted character 
                                && (i >= WRITE && i <= EXIT) && flag)                                                           // and is on the range needed for menu operation (flag == 1)
                {
                        return (int) i;                                                                                         // Returns in a normal int....What was the point of it being long.....-Andrew
                }

                else if(buffer[0] != '\n' && (*p == '\n' || *p == '\0'))                                                        // Checks for unwanted characters and return the integer otherwise
                {
                        return (int) i;
                }
        }
        return -1;                                                                                                              // If a bad value was read return a fail.
}

void DoOption(choice option)
{
    switch(option)                                                                                                              // Switch case to identify what option to use.
    {
        case WRITE:                                                                                                             // Indicates a write choice
            GetMsg();                                                                                                           // Gets the users message
            break;

        case READ:                                                                                                              // Indicates a read choice
            GetNum();                                                                                                           // Gets the number from the user
            break;

        case LIST:                                                                                                             // Indicates a list choice
            LOCAL_MSG.action = LIST;                                                                                           // Sets the local message action to list the  range of messages
            pthread_mutex_lock(&LOCAL_MSG.busy);                                                                               // Locks up the mutex so one operation gets completed.
            break;

        case EXIT:                                                                                                             // Indicates a exit choice
            LOCAL_MSG.action = LEAVE;                                                                                          // Sets the action to process an exit
            break;

        default:                                                                                                                // If all else fails...break..
            break;
    }
}
void LeaveRing(int socketfd)
{
    while(NEXT_MSG.action != CLEAR)                                                                                             // Wait for all actions to be completed before sending out a exit
        WaitOnMsg(socketfd);                                                                                                    // Wait for new messages while that happens
    
    NEXT_MSG.action = LEAVE;                                                                                                    // Setting the exit message to send to neighbor
    NEXT_MSG.token = 1;                                                                                                         // ^        ^       ^       ^
    NEXT_MSG.port_self = MYPORT;                                                                                                // ^        ^       ^       ^
    NEXT_MSG.port_other = NEIGHBOR;                                                                                             // ^        ^       ^       ^
    PassMsg(socketfd);                                                                                                          // Pass the message
}
void JoinExisting(char **argv, int socketfd)
{
    struct sockaddr_in client_addr;                                                                                             // Create a client address data structure
    socklen_t client_addrlen;                                                                                                   // Create a size variable for client structure
    udp_msg msg_in;                                                                                                             // Declare a new msg buffer
    udp_msg msg_out;                                                                                                            // Declare a new msg buffer
    ssize_t bread;                                                                                                              // Declare a variable to store the bytes read (programmer asks for another loaf)

    inet_pton(AF_INET, LOCAL, &client_addr.sin_addr);                                                                           // Setting the address to loopback for localhost
    client_addr.sin_port = htons(atoi(argv[2]));                                                                                // Setting the port to connect to a peer
    client_addr.sin_family = AF_INET;                                                                                           // Setting family name (ipv4)
    client_addrlen = sizeof(client_addr);                                                                                       // Setting size of family

    msg_out.action = JOIN;                                                                                                      // Defining join message
    msg_out.token = 0;                                                                                                          // ^    ^   ^   ^
    msg_out.port_self = MYPORT;                                                                                                 // ^    ^   ^   ^
    msg_out.port_other = 0;                                                                                                     // ^    ^   ^   ^

    bread = sendto(socketfd, &msg_out, sizeof(msg_out), 0,(struct sockaddr *) &client_addr,client_addrlen);                     // Sending message to Peer
    if (bread <= 0)                                                                                                             // If send fails
    {
        printf("Failed to send\n");                                                                                             // Print error
        exit(-1);                                                                                                               // exit
    }

    bread = recvfrom(socketfd, &msg_in, sizeof(msg_in), 0, (struct sockaddr *) &client_addr, &client_addrlen);
    if (bread <= 0)                                                                                                             // If send fails
    {
        printf("Failed to receive\n");                                                                                          // Print error
        exit(-1);                                                                                                               // exit
    }

    if(msg_in.token == 1)                                                                                                       // Set the token to 1 if it's 1
        TOKEN = 1;

    if(msg_in.action == UPDATE)                                                                                                 // Update the port to new neighbor
        NEIGHBOR = msg_in.port_other;
}
int MsgCount()
{

    FILE *fp;                                                                                                                   // Declare file stream pointer
    int ch,len,i,count = 0;                                                                                                     // Declare variables for the counting
    char *word = "<message n=";                                                                                                 // Define the search pattern

    if(NULL==(fp=fopen(LOCAL_MSG.filename, "r")))                                                                               // Open the bb file
    {
        printf("Error opening file\n");                                                                                         // Print error message if Sum Tin Wong...
        return -1;                                                                                                              // return a error
    }
    len = strlen(word);                                                                                                         // Define the size of the search literal
    while(1){
        if(EOF==(ch=fgetc(fp))) break;                                                                                          // If end of file break
        if((char)ch != *word) continue;                                                                                         // Didn't match up to the literal, continue
        for(i=1;i<len;++i){                                                                                                     // Checks the literal by character 
            if(EOF==(ch = fgetc(fp))) goto end;                                                                                 // ^ ^ ^ ^ ^ 
            if((char)ch != word[i]){                                                                                            // ^ ^ ^ ^ ^ ^
                fseek(fp, 1-i, SEEK_CUR);                                                                                       /// go to the next word and repeat
                goto next;                                                                                                      // Skip to the next word
            }
        }
        ++count;                                                                                                                // Increase found count
        next: ;                                                                                                                     
    }
    end:
        fclose(fp);                                                                                                             // Close the file pointer (courtesy is king)
    return count;                                                                                                               // Return the number found
}

void GetMsg(void)
{    
    char buff[500];                                                                                                             // Declare  the input buffer
    printf("Inserting new message.\n");                                                                                         // Printing user message
    printf("To end messsage: Press Ctrl+D (EOF) on a new line.\n:");                                                            // ^ ^ ^ ^ ^ ^ ^ 

    while(NULL != fgets(buff, 500,stdin))                                                                                       // Gets multiline message until EOF is found
    {
        strcat(LOCAL_MSG.msg,buff);                                                                                             // Concats the buffer to the main buffer for writing (later)
    }
 
    fflush(stdin);                                                                                                              // Cleans out junk in the streams
    fflush(stdout);                                                                                                             // ......nasty vermin cause issues

    LOCAL_MSG.action = WRITE;                                                                                                   // Sets the write flag
    pthread_mutex_lock(&LOCAL_MSG.busy);                                                                                        // Locks the mutex to prevent multiple actions... but you know the drill
}
void WriteMsg(void)
{


    FILE *file = fopen(LOCAL_MSG.filename, "a");                                                                                // Opens the bb file at the rear position...
    int cntr = MsgCount(),                                                                                                      // Gets the number of messages
        charsToDelete,                                                                                                          // Declare variable for later
        position;                                                                                                               //   ^^^^^

    cntr++;                                                                                                                     // increments (new msesage)

    if(cntr == 1) {                         
        fprintf(file, "<messages>");                                                                                            // Inputs the "proper" xml roots
        fprintf(file, "<message n=\"%i\">",cntr);                                                                               // Creates a node
    }
    else {
        charsToDelete = 12;                                                                                                     // If its not the first message the closing brackets need to be moved
        fseeko(file,-charsToDelete,SEEK_END);                                                                                   // Moves the file pos
        position = ftello(file);                                                                                                // ^^ ^  ^ ^ ^ 
        ftruncate(fileno(file), position);                                                                                      // ^^ ^ ^ ^ ^ ^ ^ 
        fprintf(file, "<message n=\"%i\">",cntr);                                                                               // Writes the new tag
    }
    fprintf(file, "%s",LOCAL_MSG.msg);                                                                                          // Writes the message
    fprintf(file, "</message>");                                                                                                // Writes the message
    fprintf(file, "</messages>\n");                                                                                             // Writes the message
    fclose(file);                                                                                                               // Closes the file pointer
    ClearLocalMsg(0);                                                                                                           // Cleares the local message
}
void CheckFile(void)
{
    FILE *fp = NULL;                                                                                                            // Defines the file pointer
    if(access(LOCAL_MSG.filename, F_OK) != 0)                                                                                   // Check if file exists
    {
        if((fp = fopen(LOCAL_MSG.filename, "w")) == NULL)                                                                       // Creates the file
        {
            printf("Unable to create the file %s",LOCAL_MSG.filename);                                                          // Print message if someting wrong
        }
    }
    if(fp != NULL)                                                                                                              // Closes the file stream
        fclose(fp);
}
void ReadOrListMsg(int option)
{

    xmlDoc *document;                                                                                                           // Declare xmlLib stuff
    xmlNode *root, *first_child, *node;                                                                                         // Declare xmlLib stuff
    int input = LOCAL_MSG.msg_number,                                                                                           // Define the number to search for and message count start number
                linecntr = 1;
    char * pEnd;                                                                                                                // More xmlib stuff

    document = xmlReadFile(LOCAL_MSG.filename, NULL, 0);                                                                        // Opens the bb file
    if(document == NULL){                                                                                                       // Sanity check for empty directory
        fprintf(stdout,"No messages\n");                                                                                        // Sanity is lost
        ClearLocalMsg(0);                                                                                                       // Clears local message
        return;                                                                                                                 // return
    }
    root = xmlDocGetRootElement(document);                                                                                      //create tree in memory.
    first_child = root->children;
    for (node = first_child; node; node = node->next) {                                                                         //traverse through tree's children based on selection.
        if(option == 0) {                                                                                                       //If looking to read a specific message
            xmlAttr* attribute = node->properties;                                                                              // traverse down...
            xmlChar* value = xmlNodeListGetString(node->doc, attribute->children, 1);                                           // ...further
            if(input == strtol((char*)value, &pEnd,20)) {                                                                       // Check if value is the message number
                printf("<message n=\"%s\">\n%s\n",value, xmlNodeListGetString(document, node->xmlChildrenNode,1));              // Prints the message out
                break;                                                                                                          
            }
        }
        // if(option==1) {
        //     xmlAttr* attribute = node->properties;
        //     // xmlChar* value = xmlNodeListGetString(node->doc, attribute->children, 1);
        //     // printf("<message n=\"%s\">\n", value); //list the range of valid
        // }
        linecntr++;                                                                                                             // Continue looking
    }
    if(input > linecntr && option==0) 
        printf("seq doesn't exist\n");                                                                                          // Message if not found
    else if(option == 1)
        printf("Choose from a range of 1 - %d\n",linecntr-1);                                                                   // Traverses through the entire list so the range count is found
    fprintf(stdout, "...\n");                                                                                                   // Closing message
    xmlFreeDoc(document);                                                                                                       // Closes file stream
    ClearLocalMsg(0);                                                                                                           // Clears message
}
void GetNum(void)
{
    while(LOCAL_MSG.msg_number == -1)                                                                                           // Asks user input until a valid numnber is obtained
    {
        fprintf(stdout,"Message number to read:");                                                                              // Message to user
        LOCAL_MSG.msg_number = GetOption(0);                                                                                    // Grab the input
    }
    LOCAL_MSG.action = READ;                                                                                                    // Set the action
    pthread_mutex_lock(&LOCAL_MSG.busy);                                                                                        // Prevent mischief
} 

void SanityCheck(int argc, char **argv)
{
    if(argc < 4 || argc >= 6)                                                                                                   // Verifying number of arguments
    {
        printf("Usage is %s <-new> <PortSelf> <PortOther> <BulletinBoardFileName>\nNOTE: <-new> flag is optional\n",argv[0]);   // Printing usage message
        exit(-1);                                                                                                               // Exiting
    }
    if(argc == 5)                                                                                                               // Checking 5 arguments
    {
        if(strcmp("-new",argv[1]))                                                                                              // Verifying optional flag is -new
        {
            printf("The optional flag is wrong.\n");                                                                            // Printing error message
            exit(-1);                                                                                                           // Exiting
        }
        if((atoi(argv[2]) > 65535 || atoi(argv[2]) <= 0) || (atoi(argv[3]) > 65535 || atoi(argv[3]) <= 0))                      // Checking the ports aren't out of range
        {
            printf("The ports are wrong.\n");                                                                                   // Printing error message
            exit(-1);                                                                                                           // Exiting
        }
        if(strlen(argv[4]) > 99)                                                                                                // Checks the filename/path can fit in buffer
        {
            printf("The path is too long.\n");                                                                                  // Prints error message
            exit(-1);                                                                                                           // Exiting
        }
        if(!strcmp(argv[2],argv[3]))                                                                                            // Verifies ports aren't the same
        { 
            printf("Can't connect to myself, silly... Ports must be different\n");                                              // Prints message
            exit(-1);                                                                                                           // Exiting
        }
    }
    else
    {
        if((atoi(argv[1]) > 65535 || atoi(argv[1]) <= 0) || (atoi(argv[2]) > 65535 || atoi(argv[2]) <= 0))                      // Checking the ports aren't out of range
        {
            printf("The ports are wrong.\n");                                                                                   // Printing error message
            exit(-1);                                                                                                           // Exiting
        }
        if(strlen(argv[3]) > 99)                                                                                                // Checks the filename/path can fit in buffer
        {
            printf("The path is too long.\n");                                                                                  // Prints error message
            exit(-1);                                                                                                           // Exiting
        }
        if(!strcmp(argv[1],argv[2]))                                                                                            // Verifies ports aren't the same
        { 
            printf("Can't connect to myself. Ports must be different\n");                                                       // Prints message
            exit(-1);                                                                                                           // Exiting
        }
    }

}