/************************************************************
 * File name:                                               *
 *  httpServer.c                                            *
 *                                                          *
 * Purpose:                                                 *
 *  Creates a simple non-persistent TCP HTTP server.        *
 *                                                          *
 * Authors:                                                 *
 * -----------                                              * 
 * Andrew Petrovsky                                         *
 * Taylor Lookabaugh                                        *
 *                                                          *
 * Date:                                                    *
 *  02 March 2018                                           *
 *                                                          *
 * Course:                                                  *
 *  COP4635                                                 *
 *                                                          *
 * Notes:                                                   *
 *  May cause extreme giggly girl syndrome                  *
 ***********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <arpa/inet.h>


#define BYTES 1024                                              // Defines the size of byte chunks to send files with
#define DEF_PORT "60023"                                        // Defines the default port to bind server to
#define UPDATE 'U'                                              // Define update action
#define JOIN 'J'                                                // Define join action
#define LEAVE 'L'                                               // Define leave action
#define LOCAL "127.0.0.1"                                       // Define local host

/* Struct definition for udp message */
typedef struct{
    char action;                                                // Char for action word
    int token;                                                  // Int to represent a token
    uint16_t port_self;                                         // uint for port of client
    uint16_t port_other;                                        // uint for port of neighbor
}udp_msg;


/**********************************************************
 * Function Name:                                         *
 *  InitServer                                            *
 *                                                        *
 * Description:                                           *
 *  Starts the server program by initilizing values and   *
 *  creating,binding, and listening to the socket.        *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * argc           int       I        Contains number of   *
 *                                      arguemnts passed  *
 *                                      to matching.      *
 * argv          char**     I        Containts arguemnts  *
 *                                      passed from main. *
 * socketfd       int*      O        Socket used for UDP  *
 *                                      transmission.     * 
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void InitServer(int argc, char** argv,int *socketfd);

/**********************************************************
 * Function Name:                                         *
 *  WaitOnClients                                         *
 *                                                        *
 * Description:                                           *
 *  Waits on # of clients specified by argv.              *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * argc           int       I        Contains number of   *
 *                                      arguemnts passed  *
 *                                      to matching.      *
 * argv          char**     I        Containts arguemnts  *
 *                                      passed from main. *
 * socketfd       int*      I        Socket used for UDP  *
 *                                      transmission.     * 
 * clients        int*      O        Number of clients    *
 *                                      requested access  *
 * port_list  unint16_t**   O       Port list of all the  *
 *                                      clients.          *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void WaitOnClients(char  **argv, int *socketfd, int *clients, uint16_t **port_list);

/**********************************************************
 * Function Name:                                         *
 *  FormRing                                              *
 *                                                        *
 * Description:                                           *
 *  Sends out neighbor information to clients.            *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * socketfd       int*      I        Socket used for UDP  *
 *                                      transmission.     * 
 * clients        int*      I        Number of clients    *
 *                                      requested access  *
 * port_list  unint16_t**   I       Port list of all the  *
 *                                      clients.          *
 * Function Return:                                       *
 *    N/A                                                 *
 *********************************************************/
void FormRing(int socketfd, int clients, uint16_t *port_list);
int main(int argc, char **argv)
{
    int socketfd;                                                   // Declaring variable for socket descriptor
    uint16_t *port_list = NULL;                                     // Defining a port list;
    int clients = 0;                                                // Defining client variable to keep #of clients.

    InitServer(argc,argv,&socketfd);                               // Starts and binds the server
    WaitOnClients(argv,&socketfd,&clients,&port_list);             // Wait for clients to connect
    FormRing(socketfd,clients,port_list);                          // Send neighbor information to clients

    return 0;                                                      // Returns out
}

void InitServer(int argc, char** argv,int *socketfd)
{
    char port[6];                                                   // Declare variable to store port
    struct addrinfo serv_criteria, *serv_addr, *temp_addr;          // Declaring socket structures to find the right interface
    memset(&serv_criteria, 0, sizeof(serv_criteria));               // Setting all values to 0 so there is no unset values

    if (argc < 3)                                                   // Checking correct number of arguments 
    {
        printf("Usage is %s <portNum> <numberHosts>\n",argv[0]);    // Printing usage message incase something is off
        exit(-1);                                                   // Exiting
    }
    else
       strcpy(port,argv[1]);                                        // Sets the port to the user input
    serv_criteria.ai_family = AF_INET;                              // Setting up TCP settings in socket structure
    serv_criteria.ai_socktype = SOCK_DGRAM;                         //              ^           ^
    serv_criteria.ai_flags = AI_PASSIVE;                            //              ^           ^

    if (getaddrinfo(NULL, port, &serv_criteria, &temp_addr) != 0)   // Retrieves all available internet addresses on computer
    {                                                               //  matching the criteria specified above
        printf("Unable to get correct address\n");                  // Prints error and quits if something wrong
        exit(-1);                                                   //              ^           ^
    }                                                           

    for (serv_addr = temp_addr; serv_addr!=NULL; serv_addr=serv_addr->ai_next)                  // Cycles through all available addresses to find one it can use
    {                                                                                           //              ^           ^
        *socketfd = socket (serv_addr->ai_family, serv_addr->ai_socktype, 0);                   // Attempts to create a socket on the available address
        if (*socketfd == -1){                                                                   // Checks to see if it succeded 
            printf("Failed to create socket with the chosen address. Trying next address\n");   // Prints error message if it didn't
            continue;                                                                           // continues on to the next address structure
        }                                                                                       //              ^           ^
        if (bind(*socketfd, serv_addr->ai_addr, serv_addr->ai_addrlen) != 0)                    // Tries to bind the newly created socket
            printf("Failed to bind socket on current address\n");                               // Prints error message if it can't
        else
            break;                                                                              // Breaks out of loop if it can't and will exit upon the next message
    }

    if (serv_addr == NULL)                                                                      // Checks to see if it has a valid socket/bind
    {                                                                                           //              ^           ^
        printf("Unable to find an address to create a socket and bind\n");                      // Prints a message and quits if not
        exit(-1);                                                                               //              ^           ^
    }

    printf("Server started on port number: %s Awaiting host connections...\n",port);            // Reporting port and current working directory
    freeaddrinfo(temp_addr);                                                                    // Freeing the temp addrinfo struct
}
void WaitOnClients(char  **argv,int *socketfd, int *clients, uint16_t **port_list)
{
    struct sockaddr_in client_addr;                                 // Declare a client address data structure
    socklen_t client_addrlen;                                       // Declare a client struct length variable       
    char buff[BYTES];                                               // Declare a buffer variable
    int waiting_on,                                                 // Declare two int variables for counting
        client = 0;
    ssize_t bytes_read;                                             // Declare a int variable size of bytes received

    waiting_on = *clients = atoi(argv[2]);                          // Setting number of clients to wait on
    *port_list = malloc(waiting_on * sizeof(uint16_t));             // Allocating space in array for number of clients 

    while(client < waiting_on) 
    {
        client_addrlen = sizeof(struct sockaddr_in);                                                                                     // Defining address size
        bytes_read = recvfrom(*socketfd, buff, BYTES, 0, (struct sockaddr *) &client_addr, &client_addrlen);                             // Blocks while the function waits to receive message
        if (bytes_read == -1)                                                                                                            // Verifies good transmission
            continue;                                                                                                                    // Disregards if not
        (*port_list)[client] = client_addr.sin_port;                                                                                     // If good the port number of received message is stripped
        client++;                                                                                                                        // Number of clients incremented
    }
}
void FormRing(int socketfd, int clients, uint16_t *port_list)
{
    struct sockaddr_in client_addr;                                                                                                      // Declares a client address data structure
    socklen_t client_addrlen;                                                                                                            // Declares a client address length variable 
    udp_msg out_msg;                                                                                                                     // Declares a udp message
    int count;                                                                                                                           // Declares a count variable for something...not really sure why..could be counting
    ssize_t bytes_sent;                                                                                                                  // Declares a int to keep track of bytes


    inet_pton(AF_INET, LOCAL, &client_addr.sin_addr);                                                                                    // Defines the localhost in client address
    client_addr.sin_family = AF_INET;                                                                                                    // Defines the family type in client address

    for (count = 0; count < clients; count++)                                                                                            // Starts a for loop to send out each client a message about neighbors
    {
        client_addr.sin_port = port_list[count];                                                                                         // Sets the first port
        client_addrlen = sizeof(client_addr);                                                                                            // Sets the addres size
        if(count != clients-1)                                                                                                           // Verifies its not the last client
        {
            out_msg.action = 'U';                                                                                                        // Sets the message
            out_msg.token = 0;                                                                                                           // ^ ^ ^ ^  
            out_msg.port_other = port_list[count+1];                                                                                     // ^^^^^^^
            out_msg.port_self = 0;                                                                                                       // ^ ^ ^ ^ ^ 

            bytes_sent = sendto(socketfd, &out_msg, sizeof(out_msg), 0,(struct sockaddr *) &client_addr,client_addrlen);                 // Makes a transmission
            if (bytes_sent <= 0)                                                                                                         // Checks for errors
            {
                fprintf(stderr, "Error sending response\n");                                                                             // Prints error message
                exit(-1);                                                                                                                // Exits        
            }
        }
        else 
        {
            out_msg.action = 'U';                                                                                                         // Sets the message
            out_msg.token = 1;                                                                                                            // ^      ^       ^
            out_msg.port_other = port_list[0];                                                                                            // ^      ^       ^
            out_msg.port_self = 0;                                                                                                        // ^      ^       ^

            bytes_sent = sendto(socketfd, &out_msg, sizeof(out_msg), 0,(struct sockaddr *) &client_addr,client_addrlen);                  // Transmits 
            if (bytes_sent == 0)                                                                                                          // Checks for transmit errors
            {
                fprintf(stderr, "Error sending response\n");                                                                              // Prints out error
                exit(-1);                                                                                                                 // Exits
            }
        }

    }
}